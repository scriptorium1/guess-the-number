window.addEventListener('DOMContentLoaded', function() {
	
    let tries;
    let randomNumber;
    function startGame() {
        tries = 10;
        randomNumber = Math.floor(Math.random() * 100);
       console.log('Le nombre à deviner est ' + randomNumber);
    }

    const form = document.getElementById("guess_form");

    function submitForm(event) {
        event.preventDefault();
        const value = parseInt(form.guess.value);
        comparerlesNombres(value);
            }


    function comparerlesNombres(value) {
        let messageElement = document.getElementById('message');

        if (value === randomNumber) {
            messageElement.innerHTML = `Félicitations ! Vous avez trouvé le bon nombre en ${10 - tries + 1} essais.`;
            startGame();
        } else if (value < randomNumber) {
            messageElement.innerHTML = "Le nombre recherché est plus grand. Essayez encore.";
        } else if (value > randomNumber) {
            messageElement.innerHTML = "Le nombre recherché est plus petit. Essayez encore.";
        }
    }
    form.addEventListener('submit', submitForm);
    startGame();

});
